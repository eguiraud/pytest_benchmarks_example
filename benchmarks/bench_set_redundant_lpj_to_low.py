# -*- coding: utf-8 -*-
# Benchmark setting lpjs of redundant new_states to -1e-100

from tvem.variational._utils import set_redundant_lpj_to_low
import torch as to
from numba import njit, f4, u1
import numpy as np
from cython_set_red_to_low import cython_set_red_to_low

N, S, H = 100, 64, 100
newS = 16

old_states = (to.rand(N, S, H) < 0.2).byte()
old_states_numpy = old_states.numpy()

new_states = (to.rand(N, newS, H) < 0.2).byte()
new_states[:, 0] = old_states[:, 0]  # make sure some states are the same
new_states_numpy = new_states.numpy()

new_lpj = to.ones(N, newS)
new_lpj_numpy = new_lpj.numpy()

set_redundant_lpj_to_low(new_states, new_lpj, old_states)
correct_solution = new_lpj.clone()


def reset_new_lpj():
    new_lpj = to.ones(N, newS)
    new_lpj_numpy = new_lpj.numpy()


def tvem_bench(benchmark):
    reset_new_lpj()
    benchmark(set_redundant_lpj_to_low, new_states, new_lpj, old_states)
    assert to.allclose(new_lpj, correct_solution)


def full_python(new_states, new_lpj, old_states):
    N, Snew, _ = new_states.shape
    S = old_states.shape[1]
    low_lpj = to.tensor([-np.inf], dtype=new_lpj.dtype)
    for n in range(N):  # for each datapoint
        for s in range(Snew):  # for each new state
            for ss in range(Snew):  # check if equal to other new states
                if s != ss and to.equal(new_states[n, s], new_states[n, ss]):
                    new_lpj[n, s] = low_lpj
                    break
            else:  # check if equal to an old state
                for ss in range(S):
                    if to.equal(new_states[n, s], old_states[n, ss]):
                        new_lpj[n, s] = low_lpj
                        break


def full_python_bench(benchmark):
    reset_new_lpj()
    benchmark(full_python, new_states, new_lpj, old_states)
    assert to.allclose(new_lpj, correct_solution)


@njit("void(u1[:, :, ::1], f4[:, ::1], u1[:, :, ::1])", nogil=True)
def numba(new_states, new_lpj, old_states):
    N, Snew, _ = new_states.shape
    S = old_states.shape[1]
    low_lpj = -np.inf
    for n in range(N):  # for each datapoint
        for s in range(Snew):  # for each new state
            for ss in range(Snew):  # check if equal to other new states
                if s != ss and np.equal(new_states[n, s], new_states[n, ss]).all():
                    new_lpj[n, s] = low_lpj
                    break
            else:  # check if equal to an old state
                for ss in range(S):
                    if np.equal(new_states[n, s], old_states[n, ss]).all():
                        new_lpj[n, s] = low_lpj
                        break


def numba_bench(benchmark):
    reset_new_lpj()
    benchmark(numba, new_states_numpy, new_lpj_numpy, old_states_numpy)
    assert to.allclose(to.from_numpy(new_lpj_numpy), correct_solution)


def cython_bench(benchmark):
    reset_new_lpj()
    benchmark(cython_set_red_to_low, new_states_numpy, new_lpj_numpy, old_states_numpy)
    new_lpj_numpy[
        new_lpj_numpy < -1e10
    ] = -np.inf  # cython can't set elements to -np.inf
    assert to.allclose(to.from_numpy(new_lpj_numpy), correct_solution)

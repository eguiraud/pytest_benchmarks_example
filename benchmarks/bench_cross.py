import torch as to
import pytest
from itertools import combinations
import numpy as np
import tvem


@pytest.fixture(scope="function", params=[10, 1000])
def H(request):
    return request.param


@pytest.fixture
def N(request):
    return 32


def original_cross(parents: to.Tensor) -> to.Tensor:
    device = parents.device
    n_parents, H = parents.shape
    ind_children = to.arange(2, device=device)
    children = to.empty((n_parents * (n_parents - 1), H), dtype=to.uint8, device=device)
    for p in combinations(range(n_parents), 2):
        cp = to.randint(low=1, high=H, size=(1,), device=device).item()
        children[ind_children] = parents[p, :]
        children[ind_children, cp:] = parents[p[-1::-1], cp:]
        ind_children += 2
    return children


def new_cross(parents: to.Tensor) -> to.Tensor:
    n_parents, H = parents.shape
    n_children = n_parents * (n_parents - 1)
    cp = np.random.randint(low=1, high=H, size=(n_children // 2,))
    parent_pairs = np.array(list(combinations(range(n_parents), 2)), dtype=np.int64)

    crossed_idxs = np.empty(n_children * H, dtype=np.int64)
    ii = np.arange(n_children // 2)
    off1 = ii * 2 * H
    off2 = off1 + cp
    off3 = off2 + H
    off4 = off1 + 2 * H
    for i, o1, o2, o3, o4 in zip(ii, off1, off2, off3, off4):
        parent1, parent2 = parent_pairs[i]
        crossed_idxs[o1:o2] = parent1
        crossed_idxs[o2:o3] = parent2
        crossed_idxs[o3:o4] = parent1

    crossed_idxs = crossed_idxs.reshape(n_children, H)
    children = parents[crossed_idxs, range(H)]
    return children


def new_cross_torch(parents: to.Tensor) -> to.Tensor:
    device = parents.device
    n_parents, H = parents.shape
    n_children = n_parents * (n_parents - 1)
    cp = to.randint(low=1, high=H, size=(n_children // 2,), device=device)
    parent_pairs = to.tensor(
        list(combinations(range(n_parents), 2)), dtype=to.int64, device=device
    )

    crossed_idxs = to.empty(n_children * H, dtype=to.int64, device=device)
    ii = to.arange(n_children // 2, device=device)
    off1 = ii * 2 * H
    off2 = off1 + cp
    off3 = off2 + H
    off4 = off1 + 2 * H
    for i, o1, o2, o3, o4 in zip(ii, off1, off2, off3, off4):
        parent1, parent2 = parent_pairs[i]
        crossed_idxs[o1:o2] = parent1
        crossed_idxs[o2:o3] = parent2
        crossed_idxs[o3:o4] = parent1

    crossed_idxs = crossed_idxs.reshape(n_children, H)
    children = parents[crossed_idxs, range(H)]
    return children


# ---- BENCHMARKS ---- #
def original_bench(benchmark, H, N):
    parents = (to.rand(N, H) < 0.5).to(dtype=to.uint8, device=tvem.get_device())
    benchmark(original_cross, parents)


def new_cross_bench(benchmark, H, N):
    parents = (to.rand(N, H) < 0.5).to(dtype=to.uint8, device=tvem.get_device())
    benchmark(new_cross, parents)


def torch_bench(benchmark, H, N):
    parents = (to.rand(N, H) < 0.5).to(dtype=to.uint8, device=tvem.get_device())
    benchmark(new_cross_torch, parents)


if __name__ == "__main__":
    # to allow profiling with cProfile, line_profiler
    pytest.main([__file__])

#!/bin/bash
set -euo pipefail

cython -3 -Xboundscheck=False cython_set_red_to_low.pyx

gcc -pthread -Wl,--sysroot=/ -Wsign-compare -DNDEBUG -g -fwrapv -O3 -Wall -Wstrict-prototypes -fPIC \
   -I${CONDA_PREFIX}/include/python3.7m -c cython_set_red_to_low.c -o cython_set_red_to_low.o

gcc -pthread -shared -L${CONDA_PREFIX}/lib -Wl,-rpath=${CONDA_PREFIX}/lib \
   -Wl,--no-as-needed -Wl,--sysroot=/ cython_set_red_to_low.o -o cython_set_red_to_low.cpython-37m-x86_64-linux-gnu.so

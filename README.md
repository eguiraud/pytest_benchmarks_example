# TVEM Benchmarks

A suite of benchmarks for the [TVEM](https://gitlab.com/mloldenburg/tvem) framework.

## Usage

- create the environment: `cd tvem_benchmarks && conda env create`
- activate the environment: `conda activate tvem_benchmarks`
- build cython extension module: `cd benchmarks && bash cython_make.sh && cd ..`
- run the tests with `pytest`
